PREFIX			:=	i686-linux-gnu-

CC				:=	$(PREFIX)gcc -std=c99
CPPFLAGS		:=	-I./src
CFLAGS			:=	-W -Wall -Wextra \
					-fno-stack-protector \
					-fno-builtin \
					-nostdinc

RM				:=	rm -rf

OBJS			:=	src/boot.o \
					src/main.o

KERNEL			:=	build/kernel

all: $(KERNEL) $(KERNEL).img
	mcopy -o $< c:/boot/grub/

clean:
	$(RM) $(OBJS)

fclean: clean
	$(RM) $(KERNEL) $(KERNEL).img

re: fclean all

.PHONY: all clean fclean re

$(KERNEL): $(OBJS)
	mkdir -p build
	ld --warn-common -T link.ld -o $@ $^

$(KERNEL).img:
	echo "drive c: file=\"`pwd`/$(KERNEL).img\" partition=1" > ~/.mtoolsrc
	dd if=/dev/zero of=$(KERNEL).img count=63 bs=709632
	mpartition -I c:
	mpartition -c -t 88 -h 16 -s 63 c:
	mformat c:
	mmd c:/boot
	mmd c:/boot/grub
	mcopy /usr/lib/grub/i386-pc/stage1 c:/boot/grub
	mcopy /usr/lib/grub/i386-pc/stage2 c:/boot/grub
	mcopy /usr/lib/grub/i386-pc/fat_stage1_5 c:/boot/grub
	echo "(hd0) $(KERNEL).img" > bmap
	printf "geometry (hd0) 88 16 63 \n root (hd0,0) \n setup (hd0)\n" | /usr/sbin/grub --device-map=bmap  --batch
	rm bmap
	mcopy menu.lst c:/boot/grub/