#!/usr/bin/env bash

cd $(dirname $0)/..

docker run --rm -it -v $(pwd):/app -w /app myke make re
qemu-system-i386 -hda build/kernel.img -m 512