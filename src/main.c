#define COLS 80
#define ROWS 25




#define BLACK			0x0
#define BLUE			0x1
#define GREEN			0x2
#define CYAN			0x3
#define RED				0x4
#define MAGENTA			0x5
#define BROWN			0x6
#define LIGHT_GRAY		0x7
#define DARK_GRAY		0x8
#define LIGHT_BLUE		0x9
#define LIGHT_GREEN		0xa
#define LIGHT_CYAN		0xb
#define LIGHT_RED		0xc
#define LIGHT_MAGENTA	0xd
#define YELLOW			0xe
#define WHITE			0xf

int strlen(char *str)
{
	int i;

	i = 0;
	while(str[i] != '\0')
		i++;

	return i;
}

void cmain ()
{
	unsigned short *video_ptr = (unsigned short *) 0xB8000;
	
	unsigned short fg = LIGHT_RED;
	unsigned short bg = LIGHT_BLUE;
	unsigned short ch = ' ';

// 0xABCC

	unsigned short blank = ch;
	blank |= fg << 8;
	blank |= bg << 12;

	for(int i = 0; i < ROWS; i++)
	{
		for (int j = 0; j < COLS; ++j)
		{
			video_ptr[i * COLS + j] = blank;
		}
	}

	char message[] = "Hello, world!\nMy name is Myke.";

	int i = 0;
	int j = 0;
	for (int k = 0; k < strlen(message); ++k)
	{
			unsigned short data = message[k];
			data |= fg << 8;
			data |= bg << 12;

			if (message[k] == '\n')
			{
				i+=1;
				j=0;
			}
			else
			{
				video_ptr[i * COLS + j] = data;
				j += 1;
			}
	}

    for (;;);
}
