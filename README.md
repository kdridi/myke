# Myke(rnel)

## Introduction
Myke(rnel) is a 32-bits toy kernel experiment that uses GRUB as during the bootloading process.

## Requirements
* Docker
* Qemu

## Build the Docker image
```
scripts/build-docker-image.sh
```

## Build & Run Myke(rnel)
```
scripts/run-kernel.sh
```

## Useful Links
* [https://www.gnu.org/software/grub/manual/multiboot/multiboot.html#Example-OS-code](https://www.gnu.org/software/grub/manual/multiboot/multiboot.html#Example-OS-code)
* [http://www.cs.vu.nl/~herbertb/misc/writingkernels.txt](http://www.cs.vu.nl/~herbertb/misc/writingkernels.txt)